var gulp = require('gulp');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var spritesmith = require("gulp.spritesmith");

gulp.task('default', function(){
    gulp.watch('pre-less/components_index/*.less', ['less']);
    //gulp.watch('pre-less/components/*.less', ['less']);
});

gulp.task('less', function(){
    gulp.src('pre-less/pre-less-index.less')
        .pipe(less())
        .pipe(gulp.dest('css'))
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('img/sprite/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: '../img/sprite.png',
            cssName: 'sprite.less',
            algorithm: 'alt-diagonal',
            padding: 20
        }));
    spriteData.img.pipe(gulp.dest('img'));
    spriteData.css.pipe(gulp.dest('pre-less/components'));
});

gulp.task('sprite-index', function () {
    var spriteData = gulp.src('img/sprite-index/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: '../img/sprite-index.png',
            cssName: 'sprite-index.less',
            algorithm: 'alt-diagonal',
            padding: 20
        }));
    spriteData.img.pipe(gulp.dest('img'));
    spriteData.css.pipe(gulp.dest('pre-less/components_index'));
});











