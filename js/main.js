$(document).ready(function() {
    var $valid = 0;
    var $list = $(".list");
    var $collapse = $(".collapse");
    var $part_list = $(".partisipant_list");

    function checkMail() {
        $('.mail').removeClass('notValid');
        var emailV = $('.mail').val();
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
        if (filter.test(emailV)) {
            $valid += 1;
        } else {
            $('.mail').addClass('notValid');
        }
    }

    function checkPass() {
        $('.pass').removeClass('notValid');
        var pass = $('.pass').val();
        if (pass == '' || pass == 'Password') {
            $valid = 0;
            $('.pass').addClass('notValid');
        }
        else {
            $valid += 1;
        }
    }

    $(".log_form").submit(function () {
        checkMail();
        checkPass();

        if ($valid == 2) {
            $(".sended").addClass("visible");
            return false;
        }
        else {
            return false;
        }
    });

 // collapse-delpoy participant list
    $(".collapse" ).click(function( event ) {
        if($list.hasClass("hidden")){
            $list.removeClass("hidden");
            $collapse.removeClass("deploy");
            $part_list.removeClass("hidden");
        }
        else{
            $list.addClass("hidden");
            $collapse.addClass("deploy");
            $part_list.addClass("hidden");
        }
    });
});